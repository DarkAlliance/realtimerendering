#include "RenderingGame.h"

#include <sstream>
#include <SpriteBatch.h>
#include <SpriteFont.h>

#include "GameException.h"
#include "KeyboardInput.h"
#include "MouseInput.h"
#include "FpsComponent.h"

#include "ColorHelper.h"
#include "Utility.h"
#include "FirstPersonCamera.h"
#include "Grid.h"
#include "TriangleDemo.h"
#include "Cube.h"
#include "RenderStateManager.h"
#include "TextureModelDemo.h"
namespace Rendering
{
	const XMVECTORF32 RenderingGame::BackgroundColor = { 0.392f, 0.584f, 0.929f, 1.0f };

	RenderingGame::RenderingGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand)
		: Game(instance, windowClass, windowTitle, showCommand), mFpsComponent(nullptr), mKeyboard(nullptr), mMouse(nullptr), mInput(nullptr),mModelDemo(nullptr),mCube(nullptr),mRenderStateHelper(nullptr)
	{
		mDepthStencilBufferEnabled = true;
		mMultiSamplingEnabled = true;
	}

	RenderingGame::~RenderingGame()
	{
	}

	void RenderingGame::Initialize()
	{
		if (FAILED(DirectInput8Create(mInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID*)&mInput, nullptr)))
		{
			throw GameException("DirectInput8() Create Failed!");
		}

		mKeyboard = new KeyboardInput(*this,mInput);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardInput::TypeIdClass(), mKeyboard);

		mMouse = new MouseInput(*this, mInput);
		mComponents.push_back(mMouse);
		mServices.AddService(MouseInput::TypeIdClass(), mMouse);

		mCamera = new FirstPersonCamera(*this);
		mComponents.push_back(mCamera);
		mServices.AddService(Camera::TypeIdClass(), mCamera);

		mFpsComponent = new FpsComponent(*this);
		mFpsComponent->Initialize();


		/*mDemo = new TriangleDemo(*this, *mCamera);
		mComponents.push_back(mDemo);

		mCube = new Cube(*this, *mCamera);
		mComponents.push_back(mCube);*/
		mRenderStateHelper = new RenderStateManager(*this);
		
		//mGrid = new Grid(*this, *mCamera);
		//mComponents.push_back(mGrid);

		mModelDemo = new TextureModelDemo(*this, *mCamera);
		mComponents.push_back(mModelDemo);

		Game::Initialize();
		mCamera->SetPosition(0.0f, 0.0f, 50.0f);

	}

	void RenderingGame::Update(const GameTime &gameTime)
	{
		mFpsComponent->Update(gameTime);
		if (mKeyboard->KeyPressedFrame(DIK_ESCAPE))
		{
			Exit();
		}
		Game::Update(gameTime);
	}

	void RenderingGame::Draw(const GameTime &gameTime)
	{
		mDeviceContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&BackgroundColor));
		mDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		ID3D11RasterizerState* rs;
		this->Direct3DDeviceContext()->RSGetState(&rs);
		Game::Draw(gameTime);
		mRenderStateHelper->SaveAll();
		mFpsComponent->Draw(gameTime);
		mRenderStateHelper->RestoreAll();




		HRESULT hr = mSwapChain->Present(0, 0);
		if (FAILED(hr))
		{
			throw GameException("IDXGISwapChain::Present() failed.", hr);
		}
	}
	void RenderingGame::Shutdown()
	{
		DeleteObject(mModelDemo);
		DeleteObject(mKeyboard);
		DeleteObject(mMouse)
		DeleteObject(mFpsComponent);
		
		ReleaseObject(mInput);
		
		
		Game::Shutdown();
	}
}