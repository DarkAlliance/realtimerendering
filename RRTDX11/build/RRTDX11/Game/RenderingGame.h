#pragma once
#include "Common.h"
#include "Game.h"

using namespace Library;

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}
namespace Library
{
	class FpsComponent;
	class KeyboardInput;
	class MouseInput;
	
	class FirstPersonCamera;
	class RenderStateManager;

}
namespace Rendering
{
	class TriangleDemo;
	class Cube;
	class TextureModelDemo;
	class RenderingGame : public Game
	{
	public:
		RenderingGame(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand);
		~RenderingGame();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;
	protected:
		virtual void Shutdown() override;
	private:
		static const XMVECTORF32 BackgroundColor;
		// Input 
		LPDIRECTINPUT8 mInput;
		MouseInput*	mMouse;
		KeyboardInput* mKeyboard;
		
		// Output
		TriangleDemo* mDemo;
		FirstPersonCamera * mCamera;
		Cube* mCube;
		FpsComponent* mFpsComponent;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		RenderStateManager* mRenderStateHelper;
		TextureModelDemo* mModelDemo;

	};
}
