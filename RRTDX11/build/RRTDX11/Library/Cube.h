#pragma once
#include "DrawableGameComponent.h"
using namespace Library;
namespace Rendering {
	class Cube : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(Cube, DrawableGameComponent)

	public:
		Cube(Game& game, Camera& cam);
		~Cube();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;
		float mAngle;
	private:
		typedef struct _BasicEffectVertex {
			XMFLOAT4 Position;
			XMFLOAT4 Colour;

			_BasicEffectVertex() {}
			_BasicEffectVertex(XMFLOAT4 Position, XMFLOAT4 Colour) :Position(Position), Colour(Colour) {}

		}BasicEffectVertex;


		Cube();
		Cube(const Cube& rhs);
		Cube& operator = (const Cube& rhs);
		ID3DX11Effect* mEffect;
		ID3DX11EffectTechnique* mTechnique;
		ID3DX11EffectPass* mPass;
		ID3DX11EffectMatrixVariable* mWvpVariable;
		ID3D11InputLayout* mInputLayout;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		XMFLOAT4X4 mWorldMatrix;
		XMFLOAT4X4 mTranslate;
	};

}