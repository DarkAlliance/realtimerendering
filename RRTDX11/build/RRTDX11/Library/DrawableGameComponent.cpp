#include "DrawableGameComponent.h"

#include "Camera.h"
namespace Library
{
	RTTI_DEFINITIONS(DrawableGameComponent)
	DrawableGameComponent::DrawableGameComponent() :GameComponent(), mIsVisible(true), mCam(nullptr)
	{
	}
	DrawableGameComponent::DrawableGameComponent(Game& game) : GameComponent(game), mIsVisible(true), mCam(nullptr)
	{

	}
	DrawableGameComponent::DrawableGameComponent(Game& game, Camera& camera) : GameComponent(game) ,mIsVisible(true), mCam(&camera)
	{

	}
	DrawableGameComponent::~DrawableGameComponent()
	{
	}
	bool DrawableGameComponent::Visible()const
	{
		return mIsVisible;
	}
	void DrawableGameComponent::SetVisible(bool visible)
	{
		mIsVisible = visible;
	}
	Camera* DrawableGameComponent::GetCamera()
	{
		return mCam;
	}
	void DrawableGameComponent::SetCamera(Camera* camera)
	{
		mCam = camera;
	}
	void DrawableGameComponent::Draw(const GameTime& gameTime)
	{
		
	}

}