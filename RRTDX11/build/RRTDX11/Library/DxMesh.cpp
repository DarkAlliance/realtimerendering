#include "DxMesh.h"
#include "DxModel.h"
#include "Game.h"
#include "GameException.h"
#include "scene.h"

namespace Library
{
	DxMesh::DxMesh(DxModel& model, aiMesh& mesh)
		:mModel(model),mMaterial(nullptr),mName(mesh.mName.C_Str()),mVertices(),mNormals(),mTangents(),mBiNormals(),mTextureCoordinates(),mVertexColours(),mFaceCount(0),mIndices()
	{
		mMaterial = mModel.Materials().at(mesh.mMaterialIndex);
		mVertices.reserve(mesh.mNumVertices);
		for (UINT i = 0; i < mesh.mNumVertices; i++)
		{
			mVertices.push_back(XMFLOAT3(reinterpret_cast<const float*>(&mesh.mVertices[i])));
		}
		if (mesh.HasNormals())
		{
			mNormals.reserve(mesh.mNumVertices);
			for (UINT i = 0; i < mesh.mNumVertices; i++)
			{
				mNormals.push_back(XMFLOAT3(reinterpret_cast<const float*>(&mesh.mNormals[i])));
			}
		}
		if (mesh.HasTangentsAndBitangents())
		{
			mTangents.reserve(mesh.mNumVertices);
			mBiNormals.reserve(mesh.mNumVertices);

			for (UINT i = 0; i < mesh.mNumVertices; i++)
			{
				mTangents.push_back(XMFLOAT3(reinterpret_cast<const float*>(&mesh.mTangents[i])));
				mBiNormals.push_back(XMFLOAT3(reinterpret_cast<const float*>(&mesh.mBitangents[i])));
			}
		}
		UINT uvChannelCount = mesh.GetNumUVChannels();
		for (UINT i = 0; i < uvChannelCount; i++)
		{
			std::vector<XMFLOAT3>* texCoord = new std::vector<XMFLOAT3>();
			texCoord->reserve(mesh.mNumVertices);
			mTextureCoordinates.push_back(texCoord);

			aiVector3D* aiTexCoord = mesh.mTextureCoords[i];
			for (UINT j = 0; j < mesh.mNumVertices; j++)
			{
				texCoord->push_back(XMFLOAT3(reinterpret_cast<const float*>(&aiTexCoord[j])));
			}
		}
		UINT colourChannelCount = mesh.GetNumColorChannels();
		for (UINT i = 0; i < colourChannelCount; i++)
		{
			std::vector<XMFLOAT4>* vertexColours = new std::vector<XMFLOAT4>();
			vertexColours->reserve(mesh.mNumVertices);
			mVertexColours.push_back(vertexColours);
			aiColor4D* aiColours = mesh.mColors[i];
			for (UINT j = 0; j < mesh.mNumVertices; j++)
			{
				vertexColours->push_back(XMFLOAT4(reinterpret_cast<const float*>(&aiColours[j])));
			}
		}
		if (mesh.HasFaces())
		{
			mFaceCount = mesh.mNumFaces;
			for (UINT i = 0; i < mFaceCount; i++)
			{
				aiFace* face = &mesh.mFaces[i];
				for (UINT j = 0; j < face->mNumIndices; j++)
				{
					mIndices.push_back(face->mIndices[j]);
				}
			}
		}
	}


	DxMesh::~DxMesh()
	{
		for (std::vector<XMFLOAT3>* texCoord : mTextureCoordinates)
		{
			delete texCoord;
		}
		for (std::vector<XMFLOAT4>* vertexColours : mVertexColours)
		{
			delete vertexColours;
		}
	}
	DxModel& DxMesh::GetModel()
	{
		return mModel;
	}

	DxModelMaterial* DxMesh::GetMaterial()
	{
		return mMaterial;
	}

	const std::string& DxMesh::Name() const
	{
		return mName;
	}

	const std::vector<XMFLOAT3>& DxMesh::Vertices() const
	{
		return mVertices;
	}

	const std::vector<XMFLOAT3>& DxMesh::Normals() const
	{
		return mNormals;
	}

	const std::vector<XMFLOAT3>& DxMesh::Tangents() const
	{
		return mTangents;
	}

	const std::vector<XMFLOAT3>& DxMesh::BiNormals() const
	{
		return mBiNormals;
	}

	const std::vector<std::vector<XMFLOAT3>*>& DxMesh::TextureCoordinates() const
	{
		return mTextureCoordinates;
	}

	const std::vector<std::vector<XMFLOAT4>*>& DxMesh::VertexColours() const
	{
		return mVertexColours;
	}

	UINT DxMesh::FaceCount() const
	{
		return mFaceCount;
	}
	const std::vector<UINT>& DxMesh::Indices() const
	{
		return mIndices;
	}

	void DxMesh::CreateIndexBuffer(ID3D11Buffer** indexBuffer)
	{
		assert(indexBuffer != nullptr);

		D3D11_BUFFER_DESC indexBufferDes;
		ZeroMemory(&indexBufferDes, sizeof(indexBufferDes));
		indexBufferDes.ByteWidth = sizeof(UINT) * mIndices.size();
		indexBufferDes.Usage = D3D11_USAGE_IMMUTABLE;
		indexBufferDes.BindFlags = D3D11_BIND_INDEX_BUFFER;

		D3D11_SUBRESOURCE_DATA indexSubData;
		ZeroMemory(&indexSubData, sizeof(indexSubData));
		indexSubData.pSysMem = &mIndices[0];
		if (FAILED(mModel.GetGame().Direct3DDevice()->CreateBuffer(&indexBufferDes, &indexSubData, indexBuffer)))
		{
			throw GameException("ID3D11Device::CreateBuffer() failed - DxMesh loader");
		}
	}
}