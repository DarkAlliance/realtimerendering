#pragma once
#include "Common.h"

struct aiMesh;

namespace Library
{
	class Material;
	class DxModelMaterial;


	class DxMesh
	{
		friend class DxModel;
	public:
		DxMesh(DxModel& model, DxModelMaterial* material);
		~DxMesh();

		DxModel& GetModel();
		DxModelMaterial* GetMaterial();
		const std::string& Name() const;

		const std::vector<XMFLOAT3>& Vertices() const;
		const std::vector<XMFLOAT3>& Normals() const;
		const std::vector<XMFLOAT3>& Tangents() const;
		const std::vector<XMFLOAT3>& BiNormals() const;
		const std::vector<std::vector<XMFLOAT3>*>& TextureCoordinates() const;
		const std::vector<std::vector<XMFLOAT4>*>& VertexColours() const;

		UINT FaceCount() const;
		const std::vector<UINT>& Indices()const;
		void CreateIndexBuffer(ID3D11Buffer** indexBuffer);
	private:
		DxMesh(DxModel& model, aiMesh& mesh);
		DxMesh(const DxMesh& rhs);
		DxMesh& operator=(const DxMesh& rhs);

		DxModel& mModel;
		DxModelMaterial* mMaterial;
		std::string mName;
		std::vector<XMFLOAT3> mVertices;
		std::vector<XMFLOAT3> mNormals;
		std::vector<XMFLOAT3> mTangents;
		std::vector<XMFLOAT3> mBiNormals;
		std::vector<std::vector<XMFLOAT3>*> mTextureCoordinates;
		std::vector<std::vector<XMFLOAT4>*> mVertexColours;

		UINT mFaceCount;
		std::vector<UINT> mIndices;
	};
}
