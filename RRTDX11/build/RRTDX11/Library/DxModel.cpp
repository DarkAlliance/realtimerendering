#include "DxModel.h"
#include "Game.h"
#include "GameException.h"
#include "DxMesh.h"
#include "DxModelMaterial.h"
#include "Importer.hpp"
#include "scene.h"
#include "postprocess.h"

namespace Library
{


	DxModel::DxModel(Game & game, const std::string filename, bool flipUVs) : mGame(game),mMeshes(),mMaterials()
	{
		Assimp::Importer import;
		UINT flags = aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType | aiProcess_FlipWindingOrder;
		if (flipUVs)
		{
			flags |= aiProcess_FlipUVs;
		}
		const aiScene* scene = import.ReadFile(filename, flags);
		if (scene == nullptr)
		{
			throw GameException(import.GetErrorString());
		}
		if (scene->HasMaterials())
		{
			for (UINT i = 0; i < scene->mNumMaterials; i++)
			{
				DxModelMaterial* mesh = new DxModelMaterial(*this, scene->mMaterials[i]);
				mMaterials.push_back(mesh);
			}
		}
		if (scene->HasMeshes())
		{
			for (UINT i = 0; i < scene->mNumMeshes; i++)
			{
				DxMesh* mesh = new DxMesh(*this, *(scene->mMeshes[i]));
				mMeshes.push_back(mesh);
			}
		}
	}

	DxModel::~DxModel()
	{
		for (DxMesh* mesh : mMeshes)
		{
			delete mesh;
		}
		for (DxModelMaterial* material : mMaterials)
		{
			delete material;
		}
	}
	Game& DxModel::GetGame()
	{
		return mGame;
	}
	bool DxModel::HasMeshes() const
	{
		return (mMeshes.size() > 0);
	}
	bool DxModel::HasMaterials()const
	{
		return (mMaterials.size() > 0);
	}
	const std::vector<DxMesh*>& DxModel::Meshes() const
	{
		return mMeshes;
	}
	const std::vector<DxModelMaterial*>& DxModel::Materials() const
	{
		return mMaterials;
	}
}