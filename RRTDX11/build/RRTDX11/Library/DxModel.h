#pragma once
#include "Common.h"
namespace Library
{
	class Game;
	class DxMesh;
	class DxModelMaterial;

	class DxModel
	{
	public:
		DxModel(Game& game, const std::string filename,bool fileUVs =false);
		~DxModel();

		Game& GetGame();
		bool HasMeshes() const;
		bool HasMaterials() const;

		const std::vector<DxMesh*> & Meshes() const;
		const std::vector<DxModelMaterial*>& Materials()const;

	private:
		DxModel(const DxModel& rhs);
		DxModel& operator = (const DxModel& rhs);

		Game& mGame;
		std::vector<DxMesh*> mMeshes;
		std::vector<DxModelMaterial*> mMaterials;

	};

}