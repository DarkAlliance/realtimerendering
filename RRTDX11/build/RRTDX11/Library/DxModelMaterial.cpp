#include "DxModelMaterial.h"
#include "GameException.h"
#include "Utility.h"
#include "scene.h"

namespace Library
{

	std::map<TextureType, UINT> DxModelMaterial::sTextureTypeMappings;
	DxModelMaterial::DxModelMaterial(DxModel& model) : mModel(model), mTextures()
	{
		InitializeTextureMappings();
	}
	DxModelMaterial::DxModelMaterial(DxModel& model, aiMaterial* material) : mModel(model), mTextures()
	{
		InitializeTextureMappings();
		aiString name;
		material->Get(AI_MATKEY_NAME, name);
		mName = name.C_Str();

		for (TextureType t = (TextureType)0; t < TextureTypeEnd; t = (TextureType)(t + 1))
		{
			aiTextureType mappedTexture = (aiTextureType)sTextureTypeMappings[t];

			UINT count = material->GetTextureCount(mappedTexture);
			if (count > 0)
			{
				std::vector<std::wstring>* textures = new std::vector<std::wstring>();
				mTextures.insert(std::pair<TextureType, std::vector<std::wstring>*>(t, textures));
				textures->reserve(count);
				for (UINT tIndex = 0; tIndex < count; tIndex++)
				{
					aiString path;

					if (material->GetTexture(mappedTexture, tIndex, &path) == AI_SUCCESS)
					{
						std::wstring wPath; 
						Utility::ToWideString(path.C_Str(), wPath);
						textures->push_back(wPath);
					}
				}
			}
		}
	}


	DxModelMaterial::~DxModelMaterial()
	{
		for (std::pair < TextureType, std::vector<std::wstring>*> tex : mTextures)
		{
			DeleteObject(tex.second);
		}

	}
	DxModel& DxModelMaterial::GetModel()
	{
		return mModel;
	}
	const std::string& DxModelMaterial::Name() const
	{
		return mName;
	}
	const std::map<TextureType, std::vector<std::wstring>*> DxModelMaterial::Textures() const
	{
		return mTextures;
	}
	void DxModelMaterial::InitializeTextureMappings()
	{
		if (sTextureTypeMappings.size() != TextureTypeEnd)
		{
			sTextureTypeMappings[TextureTypeDiffuse] = aiTextureType_DIFFUSE;
			sTextureTypeMappings[TextureTypeSpecularMap] = aiTextureType_SPECULAR;
			sTextureTypeMappings[TextureTypeAmbient] = aiTextureType_AMBIENT;
			sTextureTypeMappings[TextureTypeHeightmap] = aiTextureType_HEIGHT;
			sTextureTypeMappings[TextureTypeNormalMap] = aiTextureType_NORMALS;
			sTextureTypeMappings[TextureTypeSpecularPowerMap] = aiTextureType_SHININESS;
			sTextureTypeMappings[TextureTypeDisplacementMap] = aiTextureType_DISPLACEMENT;
			sTextureTypeMappings[TextureTypeLightMap] = aiTextureType_LIGHTMAP;
		}
	}
}