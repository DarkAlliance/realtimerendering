#pragma once
#include "Common.h"
struct aiMaterial;

namespace Library
{
	enum TextureType
	{
		TextureTypeDiffuse = 0,
		TextureTypeSpecularMap,
		TextureTypeAmbient,
		TextureTypeEmissive,
		TextureTypeHeightmap,
		TextureTypeNormalMap,
		TextureTypeSpecularPowerMap,
		TextureTypeDisplacementMap,
		TextureTypeLightMap,
		TextureTypeEnd

	};

	class DxModelMaterial
	{
		friend class DxModel;
	public:
		DxModelMaterial(DxModel& model);
		~DxModelMaterial();

		DxModel& GetModel();
		const std::string& Name() const;
		const std::map<TextureType, std::vector<std::wstring>*> Textures() const;
	private:
		static void InitializeTextureMappings();
		static std::map<TextureType, UINT> sTextureTypeMappings;
		DxModelMaterial(DxModel& model, aiMaterial* material);
		DxModelMaterial(const DxModelMaterial& rhs);
		DxModelMaterial& operator=(const DxModelMaterial& rhs);

		DxModel& mModel;
		std::string mName;
		std::map<TextureType, std::vector<std::wstring>*> mTextures;
	};

}