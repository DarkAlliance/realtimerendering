#include "FpsComponent.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <iomanip>
#include <sstream>
#include"Game.h"
#include "Utility.h"

namespace Library
{
	RTTI_DEFINITIONS(FpsComponent)

	FpsComponent::FpsComponent(Game& game) : DrawableGameComponent(game), 
		mSpriteBatch(nullptr),
		mSpriteFont(nullptr),
		mTextPosition(0.0f, 0.0f),
		mFrameCount(0),
		mFrameRate(0),
		mTotalElapsedTime(0.0)
	{
	}


	FpsComponent::~FpsComponent()
	{
		DeleteObject(mSpriteBatch);
		DeleteObject(mSpriteFont);
	}
	XMFLOAT2& FpsComponent::TextPosition()
	{
		return mTextPosition;
	}
	int FpsComponent::FrameRate()const
	{
		return mFrameRate;
	}
	void FpsComponent::Initialize()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		mSpriteBatch = new SpriteBatch(mGame->Direct3DDeviceContext());
		mSpriteFont = new SpriteFont(mGame->Direct3DDevice(),L"Content\\Fonts\\Arial_14_Regular.spritefont");

	}
	void FpsComponent::Update(const GameTime& gameTime)
	{
		if (gameTime.TotalGameTime() - mTotalElapsedTime >= 1)
		{
			mTotalElapsedTime = gameTime.TotalGameTime();
			mFrameRate = mFrameCount;
			mFrameCount = 0;
		}
		mFrameCount++;
	}
	void FpsComponent::Draw(const GameTime& gameTime)
	{

		mSpriteBatch->Begin();
		std::wostringstream fpsLabel;
		fpsLabel << std::setprecision(4) << L"\nFrame Rate: " << mFrameRate << "\nToal Elapsed Time: " << gameTime.TotalGameTime() << " ms";
		mSpriteFont->DrawString(mSpriteBatch, fpsLabel.str().c_str(), mTextPosition);
		mSpriteBatch->End();

	}
}