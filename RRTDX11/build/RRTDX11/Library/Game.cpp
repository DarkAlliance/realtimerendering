#include "Game.h"
#include "GameException.h"
#include "DrawableGameComponent.h"
namespace Library
{
	const UINT Game::DefaultScreenWidth = 1024;
	const UINT Game::DefaultScreenHeight = 768;
	const UINT Game::DefaultFrameRate = 60;
	const UINT Game::DefaultMultiSampleCount = 4;

	Game::Game(HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand)
		: mInstance(instance), mWindowClass(windowClass), mWindowTitle(windowTitle), mShowCommand(showCommand),
		mWindowHandle(), mWindow(),
		mScreenWidth(DefaultScreenWidth), mScreenHeight(DefaultScreenHeight),
		mGameClock(), mGameTime(),
		mFeatureLevel(D3D_FEATURE_LEVEL_10_0),
		mDevice(nullptr),
		mDeviceContext(nullptr),
		mSwapChain(nullptr),
		mDepthStencilBufferEnabled(false),
		mIsFullScreen (false),
		mMultiSamplingEnabled(false),
		mMultiSamplingCount(DefaultMultiSampleCount),
		mMultiSamplingQualityLevels(0),
		mDepthStencilBuffer(nullptr),
		mRenderTargetView(nullptr),
		mDepthStencilView(nullptr),
		mViewport(),mComponents(),
		mServices()
	{
	}

	Game::~Game()
	{
	}

	HINSTANCE Game::Instance() const
	{
		return mInstance;
	}

	HWND Game::WindowHandle() const
	{
		return mWindowHandle;
	}

	const WNDCLASSEX& Game::Window() const
	{
		return mWindow;
	}

	const std::wstring& Game::WindowClass() const
	{
		return mWindowClass;
	}

	const std::wstring& Game::WindowTitle() const
	{
		return mWindowTitle;
	}

	int Game::ScreenWidth() const
	{
		return mScreenWidth;
	}

	int Game::ScreenHeight() const
	{
		return mScreenHeight;
	}

	void Game::Run()
	{
		InitializeWindow();
		InitalizeDirectX();
		Initialize();

		MSG message;
		ZeroMemory(&message, sizeof(message));

		mGameClock.Reset();

		while (message.message != WM_QUIT)
		{
			if (PeekMessage(&message, nullptr, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
			else
			{
				mGameClock.UpdateGameTime(mGameTime);
				Update(mGameTime);
				Draw(mGameTime);
			}
		}

		Shutdown();
	}

	void Game::Exit()
	{
		PostQuitMessage(0);
	}

	void Game::Initialize()
	{
		for (GameComponent* component : mComponents) {
			component->Initialize();
		}
	}

	void Game::Update(const GameTime& gameTime)
	{
		for (GameComponent* component : mComponents)
		{
			if (component->Enabled())
			{
				component->Update(gameTime);
			}
		}
	}

	void Game::Draw(const GameTime& gameTime)
	{
		for (GameComponent* component : mComponents)
		{
			DrawableGameComponent* drawableGameComponent= component->As<DrawableGameComponent>();
			if (drawableGameComponent != nullptr && drawableGameComponent->Visible())
			{
				drawableGameComponent->Draw(gameTime);
			}
		}
	}

	ID3D11Device1 * Game::Direct3DDevice() const
	{
		return mDevice;
	}

	ID3D11DeviceContext1 * Game::Direct3DDeviceContext() const
	{
		return mDeviceContext;
	}

	bool Game::DepthBufferEnabled() const
	{
		return mDepthStencilBufferEnabled;
	}

	bool Game::IsFullScreen() const
	{
		return mIsFullScreen;
	}

	const D3D11_TEXTURE2D_DESC & Game::BackBufferDesc() const
	{
		return mBackBufferDesc;
	}

	const D3D11_VIEWPORT & Game::Viewport() const
	{
		return mViewport;
	}
	const ServiceContainer& Game::Services() const
	{
		return mServices;
	}
	float Game::AspectRatio() const
	{
		return  static_cast <float> (mScreenWidth) / mScreenHeight;
	}
	void Game::InitializeWindow()
	{
		ZeroMemory(&mWindow, sizeof(mWindow));
		mWindow.cbSize = sizeof(WNDCLASSEX);
		mWindow.style = CS_CLASSDC;
		mWindow.lpfnWndProc = WndProc;
		mWindow.hInstance = mInstance;
		mWindow.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
		mWindow.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
		mWindow.hCursor = LoadCursor(nullptr, IDC_CROSS);
		mWindow.hbrBackground = GetSysColorBrush(COLOR_BTNFACE);
		mWindow.lpszClassName = mWindowClass.c_str();

		RECT windowRectangle = { 0, 0, mScreenWidth, mScreenHeight };
		AdjustWindowRect(&windowRectangle, WS_OVERLAPPEDWINDOW, FALSE);

		RegisterClassEx(&mWindow);
		POINT center = CenterWindow(mScreenWidth, mScreenHeight);
		mWindowHandle = CreateWindow(mWindowClass.c_str(), mWindowTitle.c_str(), WS_OVERLAPPEDWINDOW, center.x, center.y, windowRectangle.right - windowRectangle.left, windowRectangle.bottom - windowRectangle.top, nullptr, nullptr, mInstance, nullptr);

		ShowWindow(mWindowHandle, mShowCommand);
		UpdateWindow(mWindowHandle);
	}

	void Game::Shutdown()
	{
		ReleaseObject(mRenderTargetView);
		ReleaseObject(mDepthStencilBuffer);
		ReleaseObject(mSwapChain);
		ReleaseObject(mDepthStencilView);

		if (mDeviceContext != nullptr)
			mDeviceContext->ClearState();

		ReleaseObject(mDevice);
		ReleaseObject(mDeviceContext);

		UnregisterClass(mWindowClass.c_str(), mWindow.hInstance);
	}
	void Game::InitalizeDirectX()
	{
		HRESULT hr;
		UINT createDeviceFlags = 0;
#if defined (DEBUG) || defined (_DEBUG)
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
		D3D_FEATURE_LEVEL featureLevels[] = {
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0

		};

		// Create Direct 3D Device an Context
		ID3D11Device* direct3DDevice = nullptr;
		ID3D11DeviceContext* direct3DDeviceContext = nullptr;

		if (FAILED(hr = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevels, ARRAYSIZE(featureLevels), D3D11_SDK_VERSION, &direct3DDevice, &mFeatureLevel, &direct3DDeviceContext)))
		{
			throw GameException("D3D11CreateDevice() Failed", hr);
		}
		if (FAILED(hr = direct3DDevice->QueryInterface(__uuidof(ID3D11Device1), reinterpret_cast<void**>(&mDevice))))
		{
			throw GameException("Failed to query ID3D11Device::Query Interface()", hr);
		}
		if (FAILED(hr = direct3DDeviceContext->QueryInterface(__uuidof(ID3D11DeviceContext1), reinterpret_cast<void**>(&mDeviceContext))))
		{
			throw GameException("Failed to query ID3D11DeviceContext::QueryInterface()", hr);
		}

		ReleaseObject(direct3DDevice);
		ReleaseObject(direct3DDeviceContext);

		// Setp 2 - Check fo multi sampling support
		mDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, mMultiSamplingCount, &mMultiSamplingQualityLevels);
		if (mMultiSamplingQualityLevels == 0)
			throw GameException("Unsupported multisampling quality");

		// Setup Swap chain description
		DXGI_SWAP_CHAIN_DESC1 swapChain;
		ZeroMemory(&swapChain, sizeof(swapChain));
		swapChain.Width	 = mScreenWidth;
		swapChain.Height = mScreenHeight;
		swapChain.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

		// Enable Multi sampling 
		if (mMultiSamplingEnabled)
		{
			swapChain.SampleDesc.Count = mMultiSamplingCount;
			swapChain.SampleDesc.Quality = mMultiSamplingQualityLevels - 1;
		}
		else // Default
		{
			swapChain.SampleDesc.Count = 1;
			swapChain.SampleDesc.Quality = 0;
		}
		swapChain.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChain.BufferCount = 1;
		swapChain.SwapEffect = DXGI_SWAP_EFFECT_DISCARD; // Quickest

		// Step 3 - Create swapchain with above description
		IDXGIDevice1* dxDevice = nullptr;
		if (FAILED(hr = mDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxDevice))))
		{
			throw GameException("Failed to query IDXGIDevice", hr);
		}
		IDXGIAdapter1* dxAdapter = nullptr;
		if (FAILED(hr = dxDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxAdapter))))
		{
			ReleaseObject(dxDevice);
			throw GameException("Failed to get DXGIADAPTER", hr);
		}
		IDXGIFactory2* dxFactory = nullptr;
		if (FAILED(hr = dxAdapter->GetParent(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxFactory))))
		{
			ReleaseObject(dxDevice);
			ReleaseObject(dxAdapter);
			throw GameException("Failed to get atapter factory", hr);
		}
		DXGI_SWAP_CHAIN_FULLSCREEN_DESC fsDesc;
		ZeroMemory(&fsDesc, sizeof(fsDesc));
		fsDesc.RefreshRate.Numerator = mFrameRate;
		fsDesc.RefreshRate.Denominator = 1;
		fsDesc.Windowed = !mIsFullScreen;

		if (FAILED(hr = dxFactory->CreateSwapChainForHwnd(dxDevice, mWindowHandle, &swapChain, &fsDesc, nullptr, &mSwapChain)))
		{
			ReleaseObject(dxDevice);
			ReleaseObject(dxAdapter);
			ReleaseObject(dxFactory);
			throw GameException("DXGI Factory - Failed to create SwapChain for HWND", hr);
		}
		ReleaseObject(dxDevice);
		ReleaseObject(dxAdapter);
		ReleaseObject(dxFactory);

		// Step 4 - Create Render Target View
		ID3D11Texture2D* backBuffer;
		if (FAILED(hr = mSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer))))
		{
			throw GameException("Failed to get buffer", hr);
		}
		backBuffer->GetDesc(&mBackBufferDesc);

		if (FAILED(hr = mDevice->CreateRenderTargetView(backBuffer, nullptr, &mRenderTargetView)))
		{
			ReleaseObject(backBuffer);
			throw GameException("Failed to create back buffer", hr);
		}
		ReleaseObject(backBuffer);

		// Create depth stencil view
		if (mDepthStencilBufferEnabled)
		{
			D3D11_TEXTURE2D_DESC dsDesc;
			ZeroMemory(&dsDesc, sizeof(dsDesc));
			dsDesc.Width = mScreenWidth;
			dsDesc.Height = mScreenHeight;
			dsDesc.MipLevels = 1;
			dsDesc.ArraySize = 1;
			dsDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			dsDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			dsDesc.Usage = D3D11_USAGE_DEFAULT;

			if (mMultiSamplingEnabled)
			{
				dsDesc.SampleDesc.Count = mMultiSamplingCount;
				dsDesc.SampleDesc.Quality = mMultiSamplingQualityLevels - 1;
			}
			else
			{
				dsDesc.SampleDesc.Count = 1;
				dsDesc.SampleDesc.Quality = 0;
			}
			if (FAILED(hr = mDevice->CreateTexture2D(&dsDesc, nullptr, &mDepthStencilBuffer)))
			{
				throw GameException("Failed to create Depth stencil texture", hr);
			}
			if (FAILED(hr = mDevice->CreateDepthStencilView(mDepthStencilBuffer, nullptr, &mDepthStencilView)))
			{
				throw GameException("Failed to create DepthStencil view", hr);
			}
			// Bind render target to Output merger 
			mDeviceContext->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);
			// Setup Viewport
			mViewport.TopLeftX = 0.0f;
			mViewport.TopLeftY = 0.0f;
			mViewport.Width = static_cast<float>(mScreenWidth);
			mViewport.Height = static_cast<float>(mScreenHeight);
			mViewport.MinDepth = 0.0f;
			mViewport.MaxDepth = 1.0f;
			mDeviceContext->RSSetViewports(1, &mViewport);
		}
	}
	LRESULT WINAPI Game::WndProc(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch (message)
		{
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		}

		return DefWindowProc(windowHandle, message, wParam, lParam);
	}

	POINT Game::CenterWindow(int windowWidth, int windowHeight)
	{
		int screenWidth = GetSystemMetrics(SM_CXSCREEN);
		int screenHeight = GetSystemMetrics(SM_CYSCREEN);

		POINT center;
		center.x = (screenWidth - windowWidth) / 2;
		center.y = (screenHeight - windowHeight) / 2;

		return center;
	}
}