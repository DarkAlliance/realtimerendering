#include "KeyboardInput.h"
#include "Game.h"
#include "GameTime.h"
#include "GameException.h"

namespace Library
{
	RTTI_DEFINITIONS(KeyboardInput)

	KeyboardInput::KeyboardInput(Game& game, LPDIRECTINPUT8 dInput): GameComponent(game),
		mInput(dInput),
		mDevice(nullptr)
	{
		assert(mInput != nullptr);
		ZeroMemory(mCurrState, sizeof(mCurrState));
		ZeroMemory(mPrevState, sizeof(mPrevState));
	}


	KeyboardInput::~KeyboardInput()
	{
		if (mDevice != nullptr)
		{
			mDevice->Unacquire();
			mDevice->Release();
			mDevice = nullptr;
		}
	}
	const byte* const KeyboardInput::CurrentState()const
	{
		return mCurrState;
	}
	const byte* const KeyboardInput::LastState() const
	{
		return mPrevState;
	}
	void KeyboardInput::Initialize()
	{
		if (FAILED(mInput->CreateDevice(GUID_SysKeyboard, &mDevice, nullptr)))
		{
			throw GameException("IDIRECTINPUT8 Create Device() Failed");
		}
		if (FAILED(mDevice->SetDataFormat(&c_dfDIKeyboard)))
		{
			throw GameException("IDIRECTINPUT8 SetDataFormat() Failed");
		}
		if (FAILED(mDevice->SetCooperativeLevel(mGame->WindowHandle(), DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
		{
			throw GameException("IDIRECTINPUT8 SetCooperationLevel() Failed");
		}
		if (FAILED(mDevice->Acquire()))
		{
			throw GameException("IDIRECTINPUT8 Aquire() Failed");
		}
	}
	void KeyboardInput::Update(const GameTime& gameTime)
	{
		if (mDevice != nullptr)
		{
			memcpy(mPrevState, mCurrState, sizeof(mCurrState));
			if (FAILED(mDevice->GetDeviceState(sizeof(mCurrState), (LPVOID)mCurrState)))
			{
				if (SUCCEEDED(mDevice->Acquire()))
				{
					mDevice->GetDeviceState(sizeof(mCurrState), (LPVOID)mCurrState);
				}
				else
				{
					//throw GameException("IDIRECTINPUT8 GetDeviceState() Failed");
				}
			}
		}
	}
	bool KeyboardInput::IsKeyUp(byte key) const
	{
		return ((mCurrState[key] & 0x80) == 0);
	}
	bool KeyboardInput::IsKeyDown(byte key) const
	{
		return ((mCurrState[key] & 0x80) != 0);
	}
	bool KeyboardInput::WasKeyUp(byte key) const
	{
		return ((mPrevState[key] & 0x80) == 0);
	}
	bool KeyboardInput::WasKeyDown(byte key)const
	{
		return ((mPrevState[key] & 0x80) != 0);
	}
	bool KeyboardInput::KeyPressedFrame(byte key) const
	{
		return (IsKeyDown(key) && WasKeyUp(key));
	}
	bool KeyboardInput::KeyReleasedFrame(byte key)const
	{
		return (IsKeyUp(key) && WasKeyDown(key));
	}
	bool KeyboardInput::IsKeyHeld(byte key) const
	{
		return (IsKeyDown(key) && WasKeyDown(key));
	}
}