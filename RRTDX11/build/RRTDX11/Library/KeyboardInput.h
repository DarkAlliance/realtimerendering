#pragma once

#include "GameComponent.h"

namespace Library
{
	class KeyboardInput :
		public GameComponent
	{
		RTTI_DECLARATIONS(KeyboardInput,GameComponent)

	public:
		KeyboardInput(Game& game, LPDIRECTINPUT8 dInput);
		~KeyboardInput();

		const byte* const CurrentState() const;
		const byte* const LastState() const;

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;

		bool IsKeyUp(byte key) const;
		bool IsKeyDown(byte key)const;
		bool WasKeyUp(byte key)const;
		bool WasKeyDown(byte key)const;
		bool KeyPressedFrame(byte key)const;
		bool KeyReleasedFrame(byte key)const;
		bool IsKeyHeld(byte key)const;

	private:
		KeyboardInput();
		static const int KeyCount = 256;
		KeyboardInput(const KeyboardInput& rhs);

		LPDIRECTINPUT8 mInput;
		LPDIRECTINPUTDEVICE8 mDevice;

		byte mCurrState[KeyCount];
		byte mPrevState[KeyCount];
	};

}