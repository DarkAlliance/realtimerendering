#include "ModelDemo.h"
#include "Game.h"
#include "GameException.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Camera.h"
#include "Utility.h"
#include "d3dcompiler.h"
#include "DxMesh.h"
#include "DxModel.h"
namespace Rendering
{

	RTTI_DEFINITIONS(ModelDemo);

	ModelDemo::ModelDemo(Game& game, Camera& camera) :
		DrawableGameComponent(game, camera), mEffect(nullptr),
		mTechnique(nullptr), mPass(nullptr), mWvpVariable(nullptr),
		mInputLayout(nullptr), mWorldMatrix(MatrixHelper::Identity),
		mVertexBuffer(nullptr), mAngle(0)
	{
	}
	ModelDemo::~ModelDemo()
	{
		ReleaseObject(mWvpVariable);
		ReleaseObject(mPass);
		ReleaseObject(mTechnique);
		ReleaseObject(mInputLayout);
		ReleaseObject(mEffect);
		ReleaseObject(mVertexBuffer);
	}
	void ModelDemo::Initialize()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		UINT shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
		shaderFlags |= D3DCOMPILE_DEBUG;
		shaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
		ID3D10Blob* compiledShader = nullptr;
		ID3D10Blob* errorMsg = nullptr;
		HRESULT hr = D3DCompileFromFile(L"Content\\Effects\\BasicEffect.fx", nullptr, nullptr, nullptr, "fx_5_0", shaderFlags, 0, &compiledShader, &errorMsg);

		if (errorMsg != nullptr)
		{
			GameException ex((char*)errorMsg->GetBufferPointer(), hr);
			//ReleaseObject(errorMsg);
			//throw ex;
		}
		if (FAILED(hr))
		{
			throw GameException("D3DX11CompileFromFile() Failed.", hr);
		}

		hr = D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), 0, mGame->Direct3DDevice(), &mEffect);
		if (FAILED(hr))
		{
			throw GameException("D3DX11CreateEffectFromMemory() Failed.", hr);
		}
		ReleaseObject(compiledShader);

		mTechnique = mEffect->GetTechniqueByName("main11");

		if (mTechnique == nullptr)
		{
			throw GameException("ID3DX11Effect::GetTechniqueByName() could not find specified technique.", hr);
		}
		mPass = mTechnique->GetPassByName("p0");
		if (mPass == nullptr)
		{
			throw GameException("ID3DX11Effect::GetTechniqueByName() could not find pass", hr);
		}
		ID3DX11EffectVariable* wvp = mEffect->GetVariableByName("WorldViewProjection");
		if (wvp == nullptr)
		{
			throw GameException("ID3DX11EffectVariable could not find Effect Variable WVP", hr);
		}
		mWvpVariable = wvp->AsMatrix();
		if (!mWvpVariable->IsValid())
		{
			throw GameException("Invalid effect var cast");
		}
		D3DX11_PASS_DESC passDes;
		mPass->GetDesc(&passDes);


		D3D11_INPUT_ELEMENT_DESC iElementDesc[] = {

			{ "POSITION", 0 , DXGI_FORMAT_R32G32B32A32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "COLOR", 0 , DXGI_FORMAT_R32G32B32A32_FLOAT,0 ,
			D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 }
		};

		if (FAILED(hr = mGame->Direct3DDevice()->CreateInputLayout(iElementDesc, ARRAYSIZE(iElementDesc), passDes.pIAInputSignature, passDes.IAInputSignatureSize, &mInputLayout)))
		{
			throw GameException("ID3DDevice::CreateInputLayout()) failed.", hr);
		}
		
		// Load up the model 
		std::string s = Utility::CurrentDirectory();
		std::unique_ptr<DxModel> model(new DxModel(*mGame, "Content\\Models\\Sphere.obj", true));

		DxMesh* mesh = model->Meshes().at(0);
		CreateVertexBuffer(mGame->Direct3DDevice(), *mesh, &mVertexBuffer);
		mesh->CreateIndexBuffer(&mIndexBuffer);
		mIndexCount = mesh->Indices().size();
	}
	void ModelDemo::Update(const GameTime & gameTime)
	{


		//mAngle += XM_PI * static_cast<float>(gameTime.ElapsedGameTime()) / 2.5f;
		//XMStoreFloat4x4(&mWorldMatrix, XMMatrixRotationX(mAngle));
		//XMStoreFloat4x4(&mWorldMatrix, XMMatrixTranslation(2.0f,0.0f, 0));

		//XMStoreFloat4x4(&mTranslate, XMMatrixTranslation(mAngle, 0.0f, 0.0f));
	}
	void ModelDemo::Draw(const GameTime & gameTime)
	{
		ID3D11DeviceContext* context = mGame->Direct3DDeviceContext();
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		context->IASetInputLayout(mInputLayout);

		UINT stride = sizeof(BasicEffectVertex);
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		XMMATRIX translate = XMLoadFloat4x4(&mTranslate);
		XMMATRIX worldMtx = XMLoadFloat4x4(&mWorldMatrix);
		//worldMtx * XMMatrixTranslation(10.0f, 0.0f, 0.0f);
		XMMATRIX wvp =  worldMtx * mCam->ViewMatrix() * mCam->ProjectionMatrix();
		mWvpVariable->SetMatrix(reinterpret_cast<const float*>(&wvp));

		mPass->Apply(0, context);
		context->DrawIndexed(mIndexCount, 0, 0);
	}
	void ModelDemo::CreateVertexBuffer(ID3D11Device * device, const DxMesh & mesh, ID3D11Buffer ** vertexBuffer) const
	{

		const std::vector<XMFLOAT3>& sourceVertices = mesh.Vertices();
		std::vector<BasicEffectVertex> vertices;
		vertices.reserve(sourceVertices.size());
		if (mesh.VertexColours().size() > 0)
		{
			std::vector<XMFLOAT4>* vertexColors = mesh.VertexColours().at(0);
			assert(vertexColors->size() == sourceVertices.size());
			for (UINT i = 0; i < sourceVertices.size(); i++)
			{
				XMFLOAT3 position = sourceVertices.at(i);
				XMFLOAT4 color = vertexColors->at(i);
				vertices.push_back(BasicEffectVertex(XMFLOAT4(position.x, position.y, position.z, 1.0f), color));

			}
		}
		else
		{
			for (UINT i = 0; i < sourceVertices.size(); i++)
			{
				XMFLOAT3 position = sourceVertices.at(i);
				XMFLOAT4 color = ColorHelper::RandomColor();

				vertices.push_back(BasicEffectVertex(XMFLOAT4(position.x, position.y, position.z, 1.0f), color));

			}
		}

		D3D11_BUFFER_DESC vertexBufferDes;
		ZeroMemory(&vertexBufferDes, sizeof(vertexBufferDes));
		vertexBufferDes.ByteWidth = sizeof(BasicEffectVertex) * vertices.size();
		vertexBufferDes.BindFlags = D3D11_USAGE_IMMUTABLE;
	
		D3D11_SUBRESOURCE_DATA vertexSubData;
		ZeroMemory(&vertexSubData, sizeof(vertexSubData));
		vertexSubData.pSysMem = &vertices[0];
		if (FAILED(device->CreateBuffer(&vertexBufferDes, &vertexSubData, vertexBuffer)))
		{
			throw GameException("ID3D11Device::CreateBuffer() failed.");
		}
	}
}