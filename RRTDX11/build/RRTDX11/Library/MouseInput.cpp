#include "MouseInput.h"
#include "Game.h"
#include "GameTime.h"
#include "GameException.h"


namespace Library
{
	RTTI_DEFINITIONS(MouseInput)

	MouseInput::MouseInput(Game& game, LPDIRECTINPUT8 dInput) :
		GameComponent(game),
		mInput(dInput),
		mDevice(nullptr),
		mX(0),
		mY(0),
		mWheel(0)
	{
		assert(mInput != nullptr);
		ZeroMemory(&mCurrState, sizeof(mCurrState));
		ZeroMemory(&mPrevState, sizeof(mPrevState));
	}


	MouseInput::~MouseInput()
	{
		if (mDevice != nullptr)
		{
			mDevice->Unacquire();
			mDevice->Release();
			mDevice = nullptr;
		}
	}
	LPDIMOUSESTATE MouseInput::CurrState()
	{
		return &mCurrState;
	}
	LPDIMOUSESTATE MouseInput::PrevState()
	{
		return &mPrevState;
	}
	long MouseInput::X() const
	{
		return mX;
	}
	long MouseInput::Y() const
	{
		return mY;
	}
	long MouseInput::Wheel()const
	{
		return mWheel;
	}
	void MouseInput::Initialize()
	{
		if (FAILED(mInput->CreateDevice(GUID_SysMouse, &mDevice, nullptr)))
		{
			throw GameException("MouseInput::CreateDevice() Failed");
		}
		if (FAILED(mDevice->SetDataFormat(&c_dfDIMouse)))
		{
			throw GameException("MouseInput::SetDataFormat() Failed");
		}
		if (FAILED(mDevice->SetCooperativeLevel(mGame->WindowHandle(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
		{
			throw GameException("MouseInput::SetCoopLvl() Failed");
		}
		if (FAILED(mDevice->Acquire()))
		{
			throw GameException("MouseInput::Aquire() Failed");
		}
	}
	void MouseInput::Update(const GameTime& gameTime)
	{
		if (mDevice != nullptr)
		{
			memcpy(&mPrevState, &mCurrState, sizeof(mCurrState));
			if (FAILED(mDevice->GetDeviceState(sizeof(mCurrState), &mCurrState)))
			{
				if (SUCCEEDED(mDevice->Acquire()))
				{
					if (FAILED(mDevice->GetDeviceState(sizeof(mCurrState), &mCurrState)))
					{
						throw GameException("MouseInput::GetDeviceState() Failed");
						return;
					}
				}
				
			}
			mX += mCurrState.lX;
			mY += mCurrState.lY;
			mWheel += mCurrState.lZ;

		}
		
	}

	bool MouseInput::IsButtonUp(MouseButtons btn) const
	{
		return ((mCurrState.rgbButtons[btn] & 0x80) == 0);
	}
	bool MouseInput::IsButtonDown(MouseButtons btn) const
	{
		return ((mCurrState.rgbButtons[btn] & 0x80) != 0);
	}
	bool MouseInput::WasButtonUp(MouseButtons btn) const
	{
		return ((mPrevState.rgbButtons[btn] & 0x80) == 0);
	}
	bool MouseInput::WasButtonDown(MouseButtons btn) const
	{
		return ((mPrevState.rgbButtons[btn] & 0x80) != 0);
	}
	bool MouseInput::WasButtonDownFrame(MouseButtons btn) const
	{
		return (IsButtonDown(btn) && WasButtonUp(btn));
	}
	bool MouseInput::WasButtonUpFrame(MouseButtons btn) const
	{
		return (IsButtonUp(btn) && WasButtonDown(btn));
	}
	bool MouseInput::IsButtonHeld(MouseButtons btn) const
	{
		return (IsButtonDown(btn) && WasButtonDown(btn));
	}

}