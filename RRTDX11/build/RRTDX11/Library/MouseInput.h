#pragma once
#include "GameComponent.h"

namespace Library
{
	class GameTime;

	enum MouseButtons {
		BtnLeft		= 0,
		BtnRight	= 1,
		BtnMiddle	= 2,
		BtnX1		= 3
	};
	class MouseInput :
		public GameComponent
	{
		RTTI_DECLARATIONS(MouseInput,GameComponent)

	public:
		MouseInput(Game& game, LPDIRECTINPUT8 dInput);
		~MouseInput();

		LPDIMOUSESTATE CurrState();
		LPDIMOUSESTATE PrevState();

		virtual void Initialize() override;
		virtual void Update(const GameTime& game) override;

		long X() const;
		long Y() const;
		long Wheel() const;

		bool IsButtonUp(MouseButtons btn)const;
		bool IsButtonDown(MouseButtons btn)const;
		bool WasButtonUp(MouseButtons btn)const;
		bool WasButtonDown(MouseButtons btn)const;
		bool WasButtonUpFrame(MouseButtons btn)const;
		bool WasButtonDownFrame(MouseButtons btn)const;
		bool IsButtonHeld(MouseButtons btn)const;

	private :
		MouseInput();
		LPDIRECTINPUT8 mInput;
		LPDIRECTINPUTDEVICE8 mDevice;
		DIMOUSESTATE mCurrState;
		DIMOUSESTATE mPrevState;

		long mX;
		long mY;
		long mWheel;
	};

}