#include "RenderStateManager.h"
#include "Game.h"

namespace Library {


	RenderStateManager::RenderStateManager(Game& game) : mGame(game), mRS(nullptr),mBS(nullptr),mBlendFactor(new FLOAT[4]),mSampleMask(UINT_MAX),mDepthStencilState(nullptr),mStencilRef(UINT_MAX)
	{
	}


	RenderStateManager::~RenderStateManager()
	{
		ReleaseObject(mRS);
		ReleaseObject(mBS);
		ReleaseObject(mDepthStencilState);
		DeleteObjects(mBlendFactor);
	}

	void RenderStateManager::ResetAll(ID3D11DeviceContext* dc)
	{
		dc->RSSetState(nullptr);
		dc->OMSetBlendState(nullptr, nullptr, UINT_MAX);
		dc->OMSetDepthStencilState(nullptr, UINT_MAX);
	}
	void RenderStateManager::SaveRasterizerState()
	{
		ReleaseObject(mRS);
		mGame.Direct3DDeviceContext()->RSGetState(&mRS);
	}
	void RenderStateManager::RestoreRasterizerState()const
	{
		mGame.Direct3DDeviceContext()->RSSetState(mRS);
	}
	void RenderStateManager::SaveBlendSate()
	{
		ReleaseObject(mBS);
		mGame.Direct3DDeviceContext()->OMGetBlendState(&mBS,mBlendFactor,&mSampleMask);
	}
	void RenderStateManager::ResoreBlendState() const
	{
		mGame.Direct3DDeviceContext()->OMSetBlendState(mBS, mBlendFactor, mSampleMask);
	}
	void RenderStateManager::SaveDetphStencilState()
	{
		ReleaseObject(mDepthStencilState);
		mGame.Direct3DDeviceContext()->OMGetDepthStencilState(&mDepthStencilState, &mStencilRef);
	}
	void RenderStateManager::RestoreDepthStencilState()const
	{
		mGame.Direct3DDeviceContext()->OMSetDepthStencilState(mDepthStencilState, mStencilRef);
	}
	void RenderStateManager::SaveAll()
	{
		SaveRasterizerState();
		SaveBlendSate();
		SaveDetphStencilState();
	}
	void RenderStateManager::RestoreAll()const
	{
		RestoreRasterizerState();
		ResoreBlendState();
		RestoreDepthStencilState();
	}
}