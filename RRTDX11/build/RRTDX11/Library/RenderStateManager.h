#pragma once
#include "Common.h"
namespace Library {

	class Game;
	class RenderStateManager
	{
	public:
		RenderStateManager(Game& game);
		~RenderStateManager();
		static void ResetAll(ID3D11DeviceContext* dc);

		ID3D11RasterizerState* RasterizerState();
		ID3D11BlendState* BlendState();
		ID3D11DepthStencilState* DepthStencilState();

		void SaveRasterizerState();
		void RestoreRasterizerState() const;

		void SaveBlendSate();
		void ResoreBlendState() const;

		void SaveDetphStencilState();
		void RestoreDepthStencilState() const;

		void SaveAll();
		void RestoreAll() const;

	private:
		RenderStateManager(const RenderStateManager& rhs);
		RenderStateManager& const operator=(const RenderStateManager& rhs);
		Game& mGame;

		ID3D11RasterizerState* mRS;
		ID3D11BlendState* mBS;
		FLOAT* mBlendFactor;
		UINT mSampleMask;
		ID3D11DepthStencilState* mDepthStencilState;
		UINT mStencilRef;


	};
}

