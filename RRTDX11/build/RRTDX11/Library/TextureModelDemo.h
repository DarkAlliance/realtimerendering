#pragma once
#include "DrawableGameComponent.h"
using namespace Library;
namespace Library
{
	class DxMesh;
}
namespace Rendering {
	class TextureModelDemo : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(TextureModelDemo, DrawableGameComponent)

	public:
		TextureModelDemo(Game& game, Camera& cam);
		~TextureModelDemo();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;
		float mAngle;
	private:
		typedef struct _TextureMappingVertex {
			XMFLOAT4 Position;
			XMFLOAT2 TextureCoordinates;

			_TextureMappingVertex() {}
			_TextureMappingVertex(XMFLOAT4 _Position, XMFLOAT2 _TextureCoordinates) :Position(_Position), TextureCoordinates(_TextureCoordinates) {}

		}TextureMappingVertex;


		TextureModelDemo();
		TextureModelDemo(const TextureModelDemo& rhs);
		TextureModelDemo& operator = (const TextureModelDemo& rhs);
		ID3DX11Effect* mEffect;
		ID3DX11EffectTechnique* mTechnique;
		ID3DX11EffectPass* mPass;
		ID3DX11EffectMatrixVariable* mWvpVariable;
		ID3DX11EffectVariable* mVariable;
		ID3D11InputLayout* mInputLayout;
		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		XMFLOAT4X4 mWorldMatrix;
		XMFLOAT4X4 mTranslate;
		XMFLOAT4X4 mRotate;
		UINT mIndexCount;
		void CreateVertexBuffer(ID3D11Device* device, const DxMesh& mesh, ID3D11Buffer** vertexBuffer) const;

		// Textures
		ID3D11ShaderResourceView* mSRV;
		ID3DX11EffectShaderResourceVariable* mColourTextureVariable;
	};

}