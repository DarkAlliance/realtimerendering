#include "TriangleDemo.h"
#include "Game.h"
#include "GameException.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Camera.h"
#include "Utility.h"
#include "d3dcompiler.h"

namespace Rendering
{

	RTTI_DEFINITIONS(TriangleDemo);

	TriangleDemo::TriangleDemo(Game& game, Camera& camera) : 
		DrawableGameComponent(game, camera), mEffect(nullptr), 
		mTechnique(nullptr), mPass(nullptr),mWvpVariable(nullptr),
		mInputLayout(nullptr), mWorldMatrix(MatrixHelper::Identity), 
		mVertexBuffer(nullptr), mAngle(0)
	{
	}
	TriangleDemo::~TriangleDemo()
	{
		ReleaseObject(mWvpVariable);
		ReleaseObject(mPass);
		ReleaseObject(mTechnique);
		ReleaseObject(mInputLayout);
		ReleaseObject(mEffect);
		ReleaseObject(mVertexBuffer);
	}
	void TriangleDemo::Initialize()
	{
		SetCurrentDirectory(Utility::ExecutableDirectory().c_str());

		UINT shaderFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
		shaderFlags |= D3DCOMPILE_DEBUG;
		shaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
		ID3D10Blob* compiledShader = nullptr;
		ID3D10Blob* errorMsg = nullptr;
		HRESULT hr = D3DCompileFromFile(L"Content\\Effects\\BasicEffect.fx", nullptr, nullptr, nullptr, "fx_5_0", shaderFlags, 0, &compiledShader, &errorMsg);

		if (errorMsg != nullptr)
		{
			GameException ex((char*)errorMsg->GetBufferPointer(), hr);
			//ReleaseObject(errorMsg);
			//throw ex;
		}
		if (FAILED(hr))
		{
			throw GameException("D3DX11CompileFromFile() Failed.", hr);
		}

		hr = D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), 0, mGame->Direct3DDevice(), &mEffect);
		if (FAILED(hr))
		{
			throw GameException("D3DX11CreateEffectFromMemory() Failed.", hr);
		}
		ReleaseObject(compiledShader);

		mTechnique = mEffect->GetTechniqueByName("main11");

		if (mTechnique == nullptr)
		{
			throw GameException("ID3DX11Effect::GetTechniqueByName() could not find specified technique.", hr);
		}
		mPass = mTechnique->GetPassByName("p0");
		if (mPass == nullptr)
		{
			throw GameException("ID3DX11Effect::GetTechniqueByName() could not find pass", hr);
		}
		ID3DX11EffectVariable* wvp = mEffect->GetVariableByName("WorldViewProjection");
		if (wvp == nullptr)
		{
			throw GameException("ID3DX11EffectVariable could not find Effect Variable WVP", hr);
		}
		mWvpVariable = wvp->AsMatrix();
		if (!mWvpVariable->IsValid())
		{
			throw GameException("Invalid effect var cast");
		}
		D3DX11_PASS_DESC passDes;
		mPass->GetDesc(&passDes);


		D3D11_INPUT_ELEMENT_DESC iElementDesc[] = {

			{"POSITION", 0 , DXGI_FORMAT_R32G32B32A32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0},
			{ "COLOR", 0 , DXGI_FORMAT_R32G32B32A32_FLOAT,0 ,
			D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 }
		};

		if (FAILED(hr = mGame->Direct3DDevice()->CreateInputLayout(iElementDesc, ARRAYSIZE(iElementDesc), passDes.pIAInputSignature, passDes.IAInputSignatureSize, &mInputLayout)))
		{
			throw GameException("ID3DDevice::CreateInputLayout()) failed.", hr);
		}
		BasicEffectVertex vertices[] =
		{
			BasicEffectVertex(
			XMFLOAT4(-1.0f,0.0f,0.0f,1.0f),
			XMFLOAT4(reinterpret_cast<const float*>(&ColorHelper::Orange))),


			BasicEffectVertex(
				XMFLOAT4(0.0f,1.0f,0.0f,1.0f),
				XMFLOAT4(reinterpret_cast<const float*>(&ColorHelper::Green))),

			BasicEffectVertex(
				XMFLOAT4(1.0f,0.0f,0.0f,1.0f),
				XMFLOAT4(reinterpret_cast<const float*>(&ColorHelper::Purple))),

		};

		D3D11_BUFFER_DESC vertexBufferDesc;
		ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));
		vertexBufferDesc.ByteWidth = sizeof(BasicEffectVertex) * ARRAYSIZE(vertices);
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA vertexSubResourceData;
		ZeroMemory(&vertexSubResourceData, sizeof(vertexSubResourceData));
		vertexSubResourceData.pSysMem = vertices;
		if (FAILED(mGame->Direct3DDevice()->CreateBuffer(&vertexBufferDesc,&vertexSubResourceData,&mVertexBuffer)))
		{
			throw GameException("ID3D11Device::CreateBuffer() failed.", hr);
		}
	}
	void TriangleDemo::Update(const GameTime & gameTime)
	{
		
		
		mAngle += XM_PI * static_cast<float>(gameTime.ElapsedGameTime()) / 2.5f;
		XMStoreFloat4x4(&mWorldMatrix, XMMatrixRotationY(mAngle));
		
	

	}
	void TriangleDemo::Draw(const GameTime & gameTime)
	{
		ID3D11DeviceContext* context = mGame->Direct3DDeviceContext();
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		context->IASetInputLayout(mInputLayout);

		UINT stride = sizeof(BasicEffectVertex);
		UINT offset = 0;
		context->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

		XMMATRIX worldMtx = XMLoadFloat4x4(&mWorldMatrix);
		XMMATRIX wvp = worldMtx * mCam->ViewMatrix() * mCam->ProjectionMatrix();
		mWvpVariable->SetMatrix(reinterpret_cast<const float*>(&wvp));

		mPass->Apply(0, context);
		context->Draw(3, 0);
	}
}